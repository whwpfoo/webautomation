use strict;
use warnings;

package WebAutomation::Engine::Naver;

use Selenium::Chrome;
use Selenium::Remote::WDKeys;
use Win32::Clipboard;
use Time::Moment;
use Try::Tiny;

use constant URL => [
    "analytics.naver.com/summary/dashboard.html",
    #"analytics.naver.com/summary/myStatistics.html", # 대시보드
    "analytics.naver.com/visit/uv.html",
    "analytics.naver.com/visit/pv.html",
    "analytics.naver.com/visit/timeline.html",
    "analytics.naver.com/visit/daily.html",
    "analytics.naver.com/visit/regionTop.html",
    "analytics.naver.com/inflow/inflowSummary.html",
    "analytics.naver.com/inflow/searchDashboard.html",
    "analytics.naver.com/inflow/keywords.html",
    "analytics.naver.com/environment/demographicsDashboard.html",
    "analytics.naver.com/environment/osDashboard.html",
    "analytics.naver.com/environment/browserDashboard.html",
    "analytics.naver.com/environment/resolutionDashboard.html",
];

sub run {
    my ($self, $id, $password, $driver, $month, $year) = @_;

    my $chrome = _get_driver($driver);

    $chrome -> get("https://nid.naver.com/nidlogin.login");
    $chrome -> pause(2000);

    # login
    try {
        my $clipboard = Win32::Clipboard -> new();
        $clipboard -> Set($id);
        $chrome -> find_element_by_id("id") -> send_keys(KEYS -> {"control"}, "v");
        $clipboard -> Set($password);
        $chrome -> find_element_by_id("pw") -> send_keys(KEYS -> {"control"}, "v");
        $chrome -> find_element_by_id("log.login") -> click();
    }
    catch {
        $chrome -> close();
        die "[!] ERROR Login Failed !: $!\n";
    };
    $chrome -> pause(4000);

    my $save_to_trust_device = $chrome -> find_element_by_id("new.dontsave");
    if ($save_to_trust_device) {
        $save_to_trust_device -> click();
        $chrome -> pause(4000);
    }

    # move page
    my ($startDate, $endDate) = _calc_period($month, $year);
    $chrome -> get("https://analytics.naver.com/summary/dashboard.html?startDateStr=$startDate&endDateStr=$endDate");
    $chrome -> pause(7000);

    my $sites = $chrome -> find_child_elements(
        $chrome -> find_element_by_id("mySiteList"), "./option"
    );
    unless ($sites) {
        $chrome -> close();
        die "[!] not exist page !\n";
    }

    # download
    print "> page count: @{$sites}\n";
    foreach (0 .. $#$sites) {
        my $target = $sites -> [$_] -> get_value();
        unless ($target) {
            next;
        }
        $chrome -> execute_script('$siteListSelectBox[0].value ="' . $target . '"');
        $chrome -> execute_script('$siteListSelectBox[0].dispatchEvent(new Event("change"));');
        $chrome -> pause(7000);
        print"> download start: $target\n";

        foreach (0 .. scalar(@{+URL}) - 1) {
            my $page = URL -> [$_];

            print ">> number[$_]: title: $page\n";
            $chrome -> get("https://$page?startDateStr=$startDate&endDateStr=$endDate");
            $chrome -> pause(7000);
            $chrome -> execute_script(_get_script_print());
            $chrome -> pause(2000);

            if ($_ == 1) {
                foreach (2 .. 3) {
                    $chrome -> find_element("//*[\@id='tablist']/label[$_]/a") -> click();
                    $chrome -> pause(6000);
                    $chrome -> execute_script(_get_script_print());
                    $chrome -> pause(2000);
                }
            }
        }
    }
    print "> complete ! system exit.\n";
    $chrome -> close();
}

sub _get_driver {
    my ($driver) = @_;
    return Selenium::Chrome -> new(
        binary             => $driver,
        auto_close         => 0,
        extra_capabilities => {
            'goog:chromeOptions' => {
                args => [
                    '--print-to-pdf',
                    '--kiosk-printing',
                    '--enable-automation',
                ],
            },
        }
    );
}

sub _calc_period {
    my ($month, $year) = @_;
    print "month: $month :: year:$year\n";
    my $start = Time::Moment -> new(
        year  => $year,
        month => $month,
        day   => 1,
    );

    my $end = $start -> plus_months(1) -> minus_days(1);
    return ($start -> strftime("%Y.%m.%d"), $end -> strftime("%Y.%m.%d"));
}

sub _get_script_print {
    return q{
                var tempTitle = document.title;
                var regionName = document.querySelector("#defaultSite").innerText.split(" ")[0];
                document.title = regionName + "_" + tempTitle;
                window.print();
                return;
            };
}

1;
