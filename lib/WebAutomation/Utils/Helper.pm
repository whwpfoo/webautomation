use strict;
use warnings;

package WebAutomation::Utils::Helper;

sub new {
    return "
            \rCore Commands
            \r==============
            \r\tCommand       Description                            Arguments
            \r\t-------       -----------                            ----------
            \r\trun           execute program                        naver|google, month(mm), year (yyyy)
            \r\thelp          print command manual                   -\n";
}

1;
