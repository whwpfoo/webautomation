use strict;
use warnings;

use lib "./lib/";
use FindBin     qw($RealBin);
use YAML        qw(LoadFile);
use Data::Dumper;

use WebAutomation::Utils::Helper;
use WebAutomation::Engine::Naver;

sub main {
    my ($command, $service, $month, $year) = @ARGV;

    if ('run' ne ($command // '')) {
        return print WebAutomation::Utils::Helper -> new();
    }

    my $config = LoadFile("$RealBin/.config/app.yml");
    my $module = $config -> {platform}{$service};

    unless ($module) {
        die "[!] ERROR required 'run' arguments.";
    }
    $module -> run(
        $config -> {id},          # login id
        $config -> {password},    # login password
        $config -> {driver},      # chrome driver path
        $month // (localtime)[4], # analytics peroid month [default: before month]
        $year // (localtime)[5] + 1900,  # analytics period year  [default: current year]
    );
}

main();
exit;
