### Summary

- 웹 드라이버를 이용한 애널리틱스 자료 다운로드 (.pdf)

---

### Download and install

```bash
  # Download
  $ git clone https://gitlab.com/whwpfoo/webautomation.git
   
  # Install libs and dependencies
  $ cpanm --installdeps . # read cpanfile and install 

  # execute script 
  $ perl webautomation.pl run [arguments..]
```

---

### Commands:

```
  COMMAND          FUNCTION
  run              execute program
  help             print command manual 
```
